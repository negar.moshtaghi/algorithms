/// Represents a node in the Trie data structure.
class TrieNode<T: Hashable> {
    var value: T?
    weak var parent: TrieNode?
    var children: [T: TrieNode] = [:]
    var isFinal: Bool = false

    /// Creates a new child node with the specified value if it doesn't already exist.
    ///
    /// - Parameter value: The value to create or retrieve a child node for.
    /// - Returns: The existing or newly created child node.
    func createIfNeeded(_ value: T) -> TrieNode {
        if let child = children[value] {
            return child
        }
        let node = TrieNode()
        children[value] = node
        return node
    }
}

/// A Trie (prefix tree) data structure for efficient storage and retrieval of elements.
class Trie<T: Hashable> {
    var root: TrieNode<T> = TrieNode()

    /// Inserts an element into the Trie.
    ///
    /// - Parameter valueArray: An array of values representing the element to insert.
    func insert(_ valueArray: [T]) {
        var node = root
        for value in valueArray {
            node = node.createIfNeeded(value)
        }
        node.isFinal = true
    }

    /// Checks if an element exists in the Trie.
    ///
    /// - Parameter valueArray: An array of values representing the element to search for.
    /// - Returns: `true` if the element exists, otherwise `false`.
    func contains(_ valueArray: [T]) -> Bool {
        var node = root
        for value in valueArray {
            guard let child = node.children[value] else { return false }
            node = child
        }
        return node.isFinal
    }

    /// Retrieves a list of nodes corresponding to the path of an element in the Trie.
    ///
    /// - Parameter valueArray: An array of values representing the element.
    /// - Returns: An array of nodes representing the path to the element, or `nil` if the element doesn't exist.
    func getNodesList(_ valueArray: [T]) -> [TrieNode<T>]? {
        var nodes: [TrieNode<T>] = []
        nodes.append(root)
        var node = root
        for value in valueArray {
            guard let child = node.children[value] else { return nil }
            node = child
            nodes.append(node)
        }
        return nodes
    }

    /// Removes an element from the Trie.
    ///
    /// - Parameter valueArray: An array of values representing the element to remove.
    /// - Returns: `true` if the element was successfully removed, otherwise `false`.
    func remove(_ valueArray: [T]) -> Bool {
        var nodes = getNodesList(valueArray)
        guard let nodes else { return false }
        var wordIndex = valueArray.count - 1
        for nodeIndex in ( 1 ..< nodes.count - 1 ).reversed() {
            let currentValue = valueArray[wordIndex]
            if nodes[nodeIndex].children[currentValue]!.children.count < 1 {
                nodes[nodeIndex].children[currentValue] = nil
            }
            wordIndex -= 1
        }
        return true
    }
}

// Example usage
let trie: Trie<Character> = Trie()
let string = Array("testt")
let string2 = Array("test")
trie.insert(string)
trie.contains(string)
trie.contains(string2)
trie.remove(string)
trie.contains(string)

