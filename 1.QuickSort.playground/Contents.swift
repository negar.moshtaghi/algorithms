/// Sorts an array using the quicksort algorithm.
///
/// - Parameters:
///   - array: The array to be sorted.
/// - Returns: A new array containing the sorted elements.
public func quicksortNaive<T: Comparable>(_ array: [T]) -> [T] {
    guard array.count > 1 else { return array }

    // Choose a pivot element (middle element in this case)
    let pivot = array[array.count / 2]

    // Divide the array into three parts: less, equal, and greater
    let less = array.filter { $0 <= pivot }
    let equal = array.filter { $0 == pivot }
    let greater = array.filter { $0 > pivot }

    // Recursively sort the less and greater parts
    return quicksortNaive(less) + equal + quicksortNaive(greater)
}

// Example usage
let array = [11, 23, 5, 66, 93, 8, 41]
let sorted = quicksortNaive(array)
print(sorted)
