# Algorithms

## 1. QuickSort
- **Time Complexity**:
    - Worst case: O(n^2) (occurs when the pivot selection leads to unbalanced partitions)
    - Average and best case: O(n log n)
- **Space Complexity**:
    - Worst case: O(n) (when the pivot selection leads to unbalanced partitions)
    - Average and best case: O(log n)

## 2. HeapSort (due to space complexity)
- **Time Complexity**:
    - Worst, average, and best case: O(N log N)
- **Space Complexity**:
    - Worst, average, and best case: O(1)

## 3. Trie
- **Time Complexity**:
    - Insertion: O(N)
    - Deletion: O(N)
    - Search: O(N)
- **Space Complexity**:
    - Insertion: O(N)
    - Deletion: O(1)
    - Search: O(1)
