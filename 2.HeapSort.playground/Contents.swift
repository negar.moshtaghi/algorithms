/// An extension on `Int` providing computed properties related to binary tree indices.
extension Int {
    /// Returns the index of the parent node in a binary tree.
    var parent: Int { (self - 1) / 2 }

    /// Returns the index of the left child node in a binary tree.
    var leftChild: Int { (self * 2) + 1 }

    /// Returns the index of the right child node in a binary tree.
    var rightChild: Int { (self * 2) + 2 }
}

extension Array where Element: Comparable {
    /// Sorts an array using the heap sort algorithm.
    ///
    /// - Parameters:
    ///   - index: The index of the current parent node in the heap.
    ///   - size: The size of the heap (number of elements).
    mutating func heapify(from index: Int, upTo size: Int) {
        var parentIndex = index
        while true {
            let leftChildIndex = parentIndex.leftChild
            let rightChildIndex = parentIndex.rightChild
            var largestIndex = parentIndex

            // Compare with left child
            if leftChildIndex < size && self[leftChildIndex] > self[largestIndex] {
                largestIndex = leftChildIndex
            }
            // Compare with right child
            if rightChildIndex < size && self[rightChildIndex] > self[largestIndex] {
                largestIndex = rightChildIndex
            }

            // If the parent is already the largest, break out of the loop
            if largestIndex == parentIndex {
                break
            }

            // Swap the parent with the largest child
            swapAt(parentIndex, largestIndex)
            parentIndex = largestIndex
        }
    }

    /// Sorts the array using the heap sort algorithm.
    mutating func heapSort() {
        let n = count

        // Build a Max Heap from the array
        for i in stride(from: n / 2 - 1, through: 0, by: -1) {
            heapify(from: i, upTo: n)
        }

        // Extract elements from the heap one by one
        for i in stride(from: n - 1, through: 1, by: -1) {
            swapAt(0, i)
            heapify(from: 0, upTo: i)
        }
    }
}

// Example usage
var array = [11, 23, 5, 66, 93, 8, 41]
array.heapSort()
print(array)
